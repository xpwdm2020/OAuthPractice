﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using OAuthPractice.ProtectedApi.Entities;

namespace OAuthPractice.ProtectedApi.Auth
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext():base("AuthContext")
        {
            
        }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
    }
}